# ws2023-algorithmenFuerPerfekteGraphen

Arbeit am Seminar "Algorithmen für perfekte Graphen" von Prof. Gurski im Wintersemester 2023/2024

## Vorlesungswebsite

Die jeweils aktuellen Informationen zur Vorlesung sind auf der [Vorlesungswebsite](https://www.cs.hhu.de/lehrstuehle-und-arbeitsgruppen/algorithmen-fuer-schwere-probleme/lehre-und-abschlussarbeiten/aktuelle-lehrveranstaltungen) zu finden:

::: ce-bodytext
Algorithmen für perfekte Graphen (Seminar)

  -------------------- -----------------------
  ** Dozent:**          Gurski
  ** Studiengang:**     Master Informatik
  ** Bereich:**         Wahlpflichtmodul
  ** Kreditpunkte:**    5
   **Anmeldung:**       im LSF ab 01.09.2023
  -------------------- -----------------------

  --------------------- ---------------- ----------------------- -------------
  ** Veranstaltung**    ** Wochentag**   ** Zeit**               ** Ort**
  Seminar (2-stündig)   Dienstag         12:30 Uhr - 14:00 Uhr   Hörsaal 5G
                        Mittwoch         10:30 Uhr - 12:00 Uhr   25.12 O2.33
  --------------------- ---------------- ----------------------- -------------

**Aktuelles:**

-   Bitte benutzen Sie zur Kommunikation mit uns immer Ihre
    Hochschul-E-Mail-Adresse.
-   Erster Vortragstermin: 12.12.2023
-   [Folien](/fileadmin/redaktion/Oeffentliche_Medien/Fakultaeten/Mathematisch-Naturwissenschaftliche_Fakultaet/Informatik/Algorithmen_Probleme/Folien.pdf)
    mit Themen und Terminen (Stand: 17.11.2023)

**Literatur:**

-   J. Bang-Jensen and G. Gutin, editors. Classes of Directed Graphs.
    Springer-Verlag, Berlin, 2018. [Springer
    Link](https://link.springer.com/book/10.1007/978-3-319-71840-8){target="_blank"}
-   A. Brandstädt and Thulasiraman, K., Arumugam, S., Brandstädt, A.,
    and Nishizeki, T., editors. Handbook of Graph Theory, Combinatorial
    Optimization, and Algorithms, chapter 29. CRC Press, 2015. [Teaching
    pdf](https://www.routledgehandbooks.com/pdf/doi/10.1201/b19163-37){target="_blank"}
-   A. Brandstädt, V.B. Le, and J.P. Spinrad. Graph Classes: A Survey.
    SIAM Monographs on Discrete Mathematics and Applications. SIAM,
    Philadelphia, 1999. [SIAM
    Epubs](https://epubs.siam.org/doi/book/10.1137/1.9780898719796){target="_blank"}
-   M.C. Golumbic. Algorithmic Graph Theory and Perfect Graphs, volume
    57 of Annals of Discrete Mathematics. Elsevier, North-Holland,
    second edition, 2004. [Science
    Direct](https://www.sciencedirect.com/bookseries/annals-of-discrete-mathematics/vol/57/){target="_blank"}
-   F. Gurski, I. Rothe, J. Rothe, and E. Wanke. Exakte Algorithmen für
    schwere Graphenprobleme. Springer-Verlag, Berlin, 2010. [Springer
    Link](https://link.springer.com/book/10.1007/978-3-642-04500-4){target="_blank"}
-   S.O. Krumke and H. Noltemeier. Graphentheoretische Konzepte und
    Algorithmen. Springer Vieweg, Stuttgart, 2012. [Springer
    Link](https://link.springer.com/book/10.1007/978-3-8348-2264-2){target="_blank"}
-   B. Korte and J. Vygen. Kombinatorische Optimierung. Springer-Verlag,
    Berlin, 2018. [Springer
    Link](https://link.springer.com/book/10.1007/978-3-662-57691-5){target="_blank"}
-   N.V.R. Mahadev and U.N. Peled. Threshold Graphs and Related Topics,
    volume 56 of Annals of Discrete Mathematics. Elsevier,
    North-Holland, 1995. [Science
    Direct](https://www.sciencedirect.com/bookseries/annals-of-discrete-mathematics/vol/56/){target="_blank"}
-   I. Rival, editor. Graphs and Order: The Role of Graphs in the Theory
    of Ordered Sets and Its Applications. Nato Science Series C: (Book
    147). Springer, 1985. [Springer
    Link](https://link.springer.com/book/10.1007/978-94-009-5315-4){target="_blank"}
-   V. Turau and C. Weyer. Algorithmische Graphentheorie. De Gruyter,
    Berlin, 2015. [De Gryter
    Online](https://www.degruyter.com/document/doi/10.1515/9783110417326/){target="_blank"}
-   L. Volkmann. Graphen an allen Ecken und Kanten. Technical report,
    RWTH Aachen, 2011. [RWTH
    Publications](https://publications.rwth-aachen.de/record/231089){target="_blank"}
