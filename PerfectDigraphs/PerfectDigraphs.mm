<map version="freeplane 1.11.5">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="Perfect Digraphs" LOCALIZED_STYLE_REF="AutomaticLayout.level.root" FOLDED="false" ID="Freemind_Link_1513112588" CREATED="1153430895318" MODIFIED="1699310499406" VGAP_QUANTITY="3 pt" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<hook NAME="MapStyle" background="#ffffff" zoom="0.592">
    <properties show_icon_for_attributes="true" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_note_icons="true" associatedTemplateLocation="template:/light_super_hero_template.mm" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_249848891" ICON_SIZE="12 pt" FORMAT_AS_HYPERLINK="false" COLOR="#000000" BACKGROUND_COLOR="#fff024" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" NUMBERED="false" FORMAT="STANDARD_FORMAT" TEXT_ALIGN="DEFAULT" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="1.9 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0" BORDER_DASH_LIKE_EDGE="true" BORDER_DASH="SOLID" VGAP_QUANTITY="3 pt" COMMON_HGAP_QUANTITY="14 pt" CHILD_NODES_LAYOUT="AUTO">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_249848891" STARTARROW="DEFAULT" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" STRIKETHROUGH="false" ITALIC="false"/>
<edge STYLE="bezier" COLOR="#09387a" WIDTH="3" DASH="SOLID"/>
<richcontent TYPE="DETAILS" CONTENT-TYPE="plain/auto"/>
<richcontent TYPE="NOTE" CONTENT-TYPE="plain/auto"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details" COLOR="#fff024" BACKGROUND_COLOR="#000000">
<font BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#f6f9a1" TEXT_ALIGN="LEFT">
<icon BUILTIN="clock2"/>
<font SIZE="10" ITALIC="true"/>
<edge COLOR="#000000"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" COLOR="#000000" BACKGROUND_COLOR="#f5131f" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#f5131f"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_254323574" STYLE="narrow_hexagon" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#c3131f">
<icon BUILTIN="yes"/>
<arrowlink COLOR="#c3131f" TRANSPARENCY="255" DESTINATION="ID_254323574"/>
<font SIZE="11" BOLD="true"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#ffffff" BACKGROUND_COLOR="#000000" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt" BORDER_WIDTH="3.1 px" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#2c2b29" BORDER_DASH_LIKE_EDGE="true">
<font NAME="Ubuntu" SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#ffffff" BACKGROUND_COLOR="#09387a" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR="#2c2b29">
<font NAME="Ubuntu" SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#ffffff" BACKGROUND_COLOR="#126cb3" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#ffffff" BACKGROUND_COLOR="#009933" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="8 pt" SHAPE_VERTICAL_MARGIN="5 pt" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#000000" BACKGROUND_COLOR="#d4e226" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" BACKGROUND_COLOR="#ffed00" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="11"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" BACKGROUND_COLOR="#fff575" BORDER_COLOR_LIKE_EDGE="true" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" BACKGROUND_COLOR="#fff575" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" BACKGROUND_COLOR="#fff575" BORDER_COLOR="#f0f0f0">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" BORDER_COLOR="#f0f0f0" BACKGROUND_COLOR="#fff575">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" BORDER_COLOR="#f0f0f0" BACKGROUND_COLOR="#fff575">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" BORDER_COLOR="#f0f0f0" BACKGROUND_COLOR="#fff575">
<font SIZE="9"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<node TEXT="Bang-Jensen et al., 2018: Chapter 11.7: Perfect Digraphs" POSITION="top_or_left" ID="ID_79740429" CREATED="1699310288120" MODIFIED="1699377201044" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="*perfect undirected graph*: chromatic number = clique number for every induced subgraph" POSITION="top_or_left" ID="ID_1008557875" CREATED="1699309622076" MODIFIED="1699310499406" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="complete biorientation (undirected edge -&gt; pair of opposing directed edges) is perfect" ID="ID_709941002" CREATED="1699309657446" MODIFIED="1699310499407" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="NP-complete problems get polynomial" POSITION="top_or_left" ID="ID_1901589061" CREATED="1699309627040" MODIFIED="1699311148757" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_381533537"/>
<node TEXT="e.g. determining chromatic, clique or independence number" ID="ID_220639347" CREATED="1699309679053" MODIFIED="1699526161884" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_336979257"/>
</node>
</node>
<node TEXT="applicable in practice: common graph classes are perfect" POSITION="top_or_left" ID="ID_640752502" CREATED="1699309735741" MODIFIED="1699310499407" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="e.g. bipartite, chordal, triangulated, interval, comparability graphs" ID="ID_604142712" CREATED="1699309757771" MODIFIED="1699310499407" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="*Strong Perfect Graph Conjecture* proven after 40 years -&gt; theorem" POSITION="top_or_left" ID="ID_1967450639" CREATED="1699309764972" MODIFIED="1699310499407" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="perfect &lt;=&gt; no odd holes / antiholes as induced subgraphs" ID="ID_1865328236" CREATED="1699309783600" MODIFIED="1699311041534" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_1602725252"/>
<node TEXT="**odd hole**: induced cycle of odd length &gt;= 5" ID="ID_1459479437" CREATED="1699309790671" MODIFIED="1699525676930" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_1455780868"/>
</node>
<node TEXT="odd antihole: complement" ID="ID_1495018257" CREATED="1699309807315" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="graphs without odd holes + antiholes: recognized in polynomial time" ID="ID_1625810566" CREATED="1699309822147" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="- Andres &amp; Hochstättler: perfect digraph class, *Strong Perfect Digraph Theorem*" POSITION="top_or_left" ID="ID_821709209" CREATED="1699309831328" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="dichromatic number instead of chromatic number for digraphs" ID="ID_382745562" CREATED="1699309839398" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**dichromatic** number: smallest number of colors k s.t. vertex coloring has no monochromatic directed cycles" POSITION="top_or_left" ID="ID_119261266" CREATED="1699309850564" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="TODO: Difference to chromatic number for digraphs?" ID="ID_1493776395" CREATED="1699309856619" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**clique** number: order of largest complete subdigraph" POSITION="top_or_left" ID="ID_1011185961" CREATED="1699309882685" MODIFIED="1699523487222" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_375329906" STARTINCLINATION="962.24997 pt;0 pt;" ENDINCLINATION="1490.99996 pt;0 pt;"/>
<arrowlink DESTINATION="ID_12697996"/>
</node>
<node TEXT="perfect directed graph: dichromatic number == clique number for any induced subgraph" POSITION="top_or_left" ID="ID_1476048667" CREATED="1699309887510" MODIFIED="1699523519920" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_322537250"/>
<node TEXT="TODO: Just &quot;any&quot; or &quot;every&quot;?" ID="ID_836576509" CREATED="1699309891513" MODIFIED="1699310499408" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="natural extension of perfectness to digraphs" ID="ID_200351723" CREATED="1699309895398" MODIFIED="1699310499409" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**symmetric part** sym(D) = spanning subdigraph containing the symmetric arcs" POSITION="top_or_left" ID="ID_982785799" CREATED="1699309900428" MODIFIED="1699525519890" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_1407529774"/>
<node TEXT="asymmetric part analogous" ID="ID_156517857" CREATED="1699309904473" MODIFIED="1699310499409" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**Strong Perfect Digraph Theorem 11.7.1**: D perfect &lt;=&gt;" POSITION="top_or_left" ID="ID_183352870" CREATED="1699309910990" MODIFIED="1699310499409" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="sym(D) perfect" ID="ID_1806727131" CREATED="1699309912736" MODIFIED="1699349563108" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_1865029363"/>
</node>
<node TEXT="no directed cycles &gt;= 3 as induced subdigraph" ID="ID_295833058" CREATED="1699309920215" MODIFIED="1699310499409" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**filled odd hole**: sym(D) is *complete biorientation* of odd hole" POSITION="top_or_left" ID="ID_816384941" CREATED="1699309926356" MODIFIED="1699310499410" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="filled odd *anti*hole analogous" ID="ID_581996315" CREATED="1699309928466" MODIFIED="1699310499410" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**corollary 11.7.2**: perfect &lt;=&gt; no filled odd holes/antiholes, no cycles &gt;= 3 as induced subdigraphs" POSITION="top_or_left" ID="ID_402006911" CREATED="1699309938215" MODIFIED="1699310499410" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="using Strong Perfect *Graph* Theorem" ID="ID_759245718" CREATED="1699309940419" MODIFIED="1699310499410" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="perfect digraph: *symmetric part* determines validity of k-dicoloring" POSITION="top_or_left" ID="ID_644548327" CREATED="1699309946616" MODIFIED="1699523206693" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="**corollary 11.7.3**: perfect digraph: k-dicoloring of sym(D) =&gt; k-dicoloring of D" ID="ID_102664348" CREATED="1699309955106" MODIFIED="1699525857654" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_1569891351"/>
</node>
</node>
<node TEXT="maximum order of induced acyclic subdigraph depends solely on sym(D)" POSITION="top_or_left" ID="ID_42602252" CREATED="1699309961439" MODIFIED="1699310499410" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="**corollary 11.7.4**: determining chromatic &amp; clique number and maximum order of induced acyclic subdigraph is polynomial" ID="ID_336979257" CREATED="1699309963294" MODIFIED="1699310499411" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**problem 11.7.5** other NP-complete problems that get polynomial for perfect digraphs?" POSITION="top_or_left" ID="ID_1855230960" CREATED="1699309969494" MODIFIED="1699310499411" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="**problem 11.7.10** NP-(co)-complete problems that remain?" POSITION="top_or_left" ID="ID_899499869" CREATED="1699309976898" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="perfect digraph share many favourable properties of perfect graphs, but" POSITION="top_or_left" ID="ID_597617301" CREATED="1699309982937" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="**theorem 11.7.6**: deciding if digraph is perfect is co-NP-complete" ID="ID_1129010749" CREATED="1699309985165" MODIFIED="1700054054492" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DESTINATION="ID_1910677533"/>
<arrowlink DESTINATION="ID_1052112696"/>
</node>
<node TEXT="**theorem 11.7.7**: deciding if perfect digraph has kernel is NP-complete" ID="ID_1153959872" CREATED="1699309989778" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**corollary 11.7.8**: complement of perfect digraph is kernel-perfect (every induced subdigraph has kernel)" POSITION="top_or_left" ID="ID_1499773327" CREATED="1699309995139" MODIFIED="1699310789846" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink DASH="2 7" FONT_SIZE="20" DESTINATION="ID_1403404002" MIDDLE_LABEL="???"/>
</node>
<node TEXT="11.7.7 + 11.7.8 =&gt; complements of perfect digraphs not necessarily perfect (Figure 11.8(a) + (d))" POSITION="top_or_left" ID="ID_1881683168" CREATED="1699310002703" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="undirected: *Weak Perfect Graph Theorem*" ID="ID_1246288493" CREATED="1699310026192" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**theorem 11.7.9**: D perfect &lt;=&gt; complement(D) is biorientation of perfect G, no induced cliques in G by cycles in asym(complement(D))" POSITION="top_or_left" ID="ID_774504082" CREATED="1699310035061" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="for undirected G: complement(biorientation(G)) = complement(G)" POSITION="top_or_left" ID="ID_607659237" CREATED="1699310040769" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="11.7.9 generalization of *Weak Perfect Graph Theorem*" ID="ID_259868896" CREATED="1699310047689" MODIFIED="1699310499412" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**maker-breaker game**: players take turns coloring vertices" POSITION="top_or_left" ID="ID_1464157740" CREATED="1699310058858" MODIFIED="1699523389854" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="no two adjacent vertices same color" ID="ID_480885284" CREATED="1699310062771" MODIFIED="1699523389858" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="stop if properly colored or no next proper coloring possible" ID="ID_309758642" CREATED="1699310063644" MODIFIED="1699523389861" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="**game chromatic number** Xg(G) smallest number of colors with winning strategy, max |V(G)|" ID="ID_1608942708" CREATED="1699310067684" MODIFIED="1699523389865" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="*Andres for digraphs*: inbound vertices only" ID="ID_1357166937" CREATED="1699310071029" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="natural definition, as Xg(G) = Xg(biorientation(G))" ID="ID_1960920240" CREATED="1699310077085" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="*Yang and Zhu*: avoid monochromatic cycles" ID="ID_1205279655" CREATED="1699310082060" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="**weak game chromatic number** / **game dichromatic number**" ID="ID_1439962041" CREATED="1699310088419" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**game-perfect** &lt;=&gt; game chromatic number = clique number" ID="ID_1572640150" CREATED="1699310094298" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="**weakly game-perfect** analogously" ID="ID_499687585" CREATED="1699310097996" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="game-perfect digraphs proper subclass" ID="ID_1446439061" CREATED="1699310103446" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
<node TEXT="**theorem 11.7.11**: game-perfect =&gt; kernel-perfect" POSITION="top_or_left" ID="ID_1631051763" CREATED="1699310110934" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="**problem 11.7.12**: set of forbidden induced subdigraphs for game-perfect digraphs" POSITION="top_or_left" ID="ID_1962846556" CREATED="1699310118368" MODIFIED="1699310499413" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="**theorem 11.7.13**: D weakly game-perfect &lt;=&gt; undirected sym(D) game-perfect and no directed cycles &gt;= 3" POSITION="top_or_left" ID="ID_62503653" CREATED="1699310123150" MODIFIED="1699310499414" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="**corollary 11.7.14**: D weakly game-perfect &lt;=&gt; no directed cycle &gt;=3 and no C4, P4, triangle star, E-graph, two double-fans, two split 3-stars" ID="ID_1536203067" CREATED="1699310129058" MODIFIED="1699310499414" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
</node>
</node>
<node TEXT="Andres and Hochstättler, 2015: Perfect Digraphs" POSITION="bottom_or_right" ID="ID_660993033" CREATED="1699310325257" MODIFIED="1699377157421" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<node TEXT="0 Abstract" ID="ID_740967209" CREATED="1699368289848" MODIFIED="1699368295043">
<node TEXT="clique number = largest bidirectionally complete subdigraph" POSITION="bottom_or_right" ID="ID_375329906" CREATED="1699310359562" MODIFIED="1699310499406" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW"/>
<node TEXT="perfect: dichromatic number == clique number for any induced subdigraph" POSITION="bottom_or_right" ID="ID_735510158" CREATED="1699310401345" MODIFIED="1699310776203" CHILD_NODES_LAYOUT="TOPTOBOTTOM_RIGHT_FLOW">
<arrowlink FONT_SIZE="20" DESTINATION="ID_1476048667"/>
</node>
<node TEXT="recognition of perfect digraphs is co-NP-complete" POSITION="bottom_or_right" ID="ID_1910677533" CREATED="1699310549217" MODIFIED="1699310571764"/>
<node TEXT="perfect digraphs == complements of acyclic superorientations of perfect graphs" POSITION="bottom_or_right" ID="ID_658425525" CREATED="1699310615303" MODIFIED="1699349468720">
<node TEXT="TODO: ???" ID="ID_358879766" CREATED="1699310634976" MODIFIED="1699310640679"/>
</node>
<node TEXT="complements of perfect digraphs have kernel" POSITION="bottom_or_right" ID="ID_1403404002" CREATED="1699310641012" MODIFIED="1699310652643">
<node TEXT="deciding if perfect digraph has kernel is NP-complete" ID="ID_904260749" CREATED="1699310664443" MODIFIED="1699310724764">
<arrowlink DESTINATION="ID_1153959872"/>
</node>
</node>
</node>
<node TEXT="1 Introduction" ID="ID_671511510" CREATED="1699368250740" MODIFIED="1699368256549">
<node TEXT="Berge graph: no odd holes / antiholes" POSITION="bottom_or_right" ID="ID_1602725252" CREATED="1699310851966" MODIFIED="1699310857657">
<node TEXT="Berge&apos;s Conjecture: perfect &lt;=&gt; Berge graph" ID="ID_434486036" CREATED="1699310860063" MODIFIED="1699310895402">
<node TEXT="partial results: e.g. (Weak) Perfect Graph Theorem" ID="ID_420122453" CREATED="1699310901180" MODIFIED="1699310954836"/>
<node TEXT="Chudnovsky et al., 2006: Strong Perfect Graph Theorem" ID="ID_1925006450" CREATED="1699310929991" MODIFIED="1699310947191"/>
</node>
</node>
<node TEXT="NP-complete problems get polynomial" POSITION="bottom_or_right" ID="ID_381533537" CREATED="1699311062148" MODIFIED="1699311083700">
<node TEXT="maximum clique problem" ID="ID_1117607224" CREATED="1699311085042" MODIFIED="1699311093909"/>
<node TEXT="maximum stable set problem" ID="ID_1610496019" CREATED="1699311094082" MODIFIED="1699311097072"/>
<node TEXT="graph coloring problem" ID="ID_1021796352" CREATED="1699311097363" MODIFIED="1700054012963">
<arrowlink DESTINATION="ID_1355422119"/>
</node>
<node TEXT="minimum clique covering problem" ID="ID_807012122" CREATED="1699311103369" MODIFIED="1699311108058"/>
</node>
<node TEXT="applicable: members of many important classes are perfect" POSITION="bottom_or_right" ID="ID_424405072" CREATED="1699311151702" MODIFIED="1699311207717">
<arrowlink DESTINATION="ID_640752502"/>
<node TEXT="bipartite + line graphs" ID="ID_47069371" CREATED="1699311180528" MODIFIED="1699311186669"/>
<node TEXT="split graphs" ID="ID_56475965" CREATED="1699311187081" MODIFIED="1699311189731"/>
<node TEXT="chordal graphs" ID="ID_1851324172" CREATED="1699311189883" MODIFIED="1699311192921"/>
<node TEXT="comparability graphs" ID="ID_1024864211" CREATED="1699311193054" MODIFIED="1699311196704"/>
</node>
<node TEXT="Dichromatic number instead of chromatic number" POSITION="bottom_or_right" ID="ID_544666636" CREATED="1699345607290" MODIFIED="1699345628562">
<arrowlink DESTINATION="ID_382745562"/>
</node>
<node TEXT="induced directed circles &gt;= 3, sym(D) perfect" POSITION="bottom_or_right" ID="ID_1865029363" CREATED="1699345714156" MODIFIED="1699345734638">
<node TEXT="forbidden subdigraphs" ID="ID_1721283150" CREATED="1699345763192" MODIFIED="1699345781449"/>
</node>
<node TEXT="2-coloring NP-complete, k-coloring for perfect digraphs in P" POSITION="bottom_or_right" ID="ID_1628780084" CREATED="1699345834143" MODIFIED="1699345861834"/>
<node TEXT="maximum induced acyclic subdigraph polynomial" POSITION="bottom_or_right" ID="ID_1308526401" CREATED="1699345948809" MODIFIED="1699526048050">
<arrowlink DESTINATION="ID_1253193047"/>
<node TEXT="symmetric digraphs: stable set" ID="ID_1360948186" CREATED="1699345981248" MODIFIED="1699345994139"/>
</node>
<node TEXT="Berge graph recognition in P" POSITION="bottom_or_right" ID="ID_237845306" CREATED="1699345996339" MODIFIED="1699349217810">
<node TEXT="SPGT: Same for (TODO: undirected?) perfect graphs" ID="ID_1324525890" CREATED="1699349217815" MODIFIED="1699349286070"/>
</node>
<node TEXT="recognition of induced directed cycles &gt;= 3 NP-complete" POSITION="bottom_or_right" ID="ID_1026631376" CREATED="1699349231603" MODIFIED="1699349311589">
<node TEXT="recognition of perfect digraphs is co-NP-complete" ID="ID_809752901" CREATED="1699349383028" MODIFIED="1699349428050">
<arrowlink DESTINATION="ID_1910677533"/>
</node>
</node>
</node>
<node TEXT="2 Notation" ID="ID_1161999992" CREATED="1699368260280" MODIFIED="1699368264157">
<node TEXT="dichromatic number" POSITION="bottom_or_right" ID="ID_704895732" CREATED="1699349630294" MODIFIED="1699523814078">
<arrowlink SHAPE="CUBIC_CURVE" DESTINATION="ID_119261266"/>
</node>
<node TEXT="clique: completely connected subdigraph" POSITION="bottom_or_right" ID="ID_1677017673" CREATED="1699349658478" MODIFIED="1699349726514">
<node TEXT="clique number: Size of largest clique" ID="ID_12697996" CREATED="1699354453127" MODIFIED="1699354461077">
<node TEXT="lower bound for dichromatic number" ID="ID_1730297848" CREATED="1699354483310" MODIFIED="1699354489481">
<node TEXT="perfect: dichromatic number == clique number" ID="ID_719263445" CREATED="1699354503636" MODIFIED="1699354523510">
<arrowlink DESTINATION="ID_735510158"/>
</node>
</node>
<node TEXT="\latex \omega" ID="ID_1374950920" CREATED="1699357641077" MODIFIED="1699357644049"/>
</node>
</node>
<node TEXT="symmetric digraph: replace undirected edges by pair" POSITION="bottom_or_right" ID="ID_572160712" CREATED="1699356676428" MODIFIED="1699356763499">
<node TEXT="dichromatic number = chromatic number" ID="ID_322537250" CREATED="1699356787041" MODIFIED="1699356793233"/>
<node TEXT="digraph perfect if graph perfect" ID="ID_1380256882" CREATED="1699356804575" MODIFIED="1699356822086"/>
<node TEXT="clique number unaffected" ID="ID_859878834" CREATED="1699356822645" MODIFIED="1699356843389"/>
</node>
<node TEXT="edge: two antiparallel arcs" POSITION="bottom_or_right" ID="ID_595098188" CREATED="1699356917238" MODIFIED="1699356926642">
<node TEXT="symmetric part S(D): all edges" POSITION="bottom_or_right" ID="ID_1407529774" CREATED="1699357079701" MODIFIED="1699357313360"/>
</node>
<node TEXT="single arc: arc without counterpart" POSITION="bottom_or_right" ID="ID_979543590" CREATED="1699356927122" MODIFIED="1699356933841">
<node TEXT="oriented part O(D): all single arcs" POSITION="bottom_or_right" ID="ID_683819762" CREATED="1699357067912" MODIFIED="1699357308611"/>
</node>
<node TEXT="**Observation 1**: clique number(D) = clique number(S(D))" POSITION="bottom_or_right" ID="ID_795488209" CREATED="1699356771484" MODIFIED="1699368505395"/>
<node TEXT="(loopless) complement: all edges that are not in A" POSITION="bottom_or_right" ID="ID_1479873283" CREATED="1699357633583" MODIFIED="1699357727531">
<node TEXT="\latex \overline D" ID="ID_212179350" CREATED="1699357740980" MODIFIED="1699357750087"/>
</node>
<node TEXT="underlying graph G(D): vw in G(D) &lt;=&gt; (v,w) or (w,v) or both in D" POSITION="bottom_or_right" ID="ID_922934645" CREATED="1699357776816" MODIFIED="1699357869504">
<node TEXT="**Observation 2**: complement(S(D)) = G(complement(D))" ID="ID_1900808479" CREATED="1699357871439" MODIFIED="1699368529997"/>
</node>
<node TEXT="G = G(D): D is superorientation of G" POSITION="bottom_or_right" ID="ID_1903977243" CREATED="1699357917447" MODIFIED="1699357933408"/>
<node TEXT="clique-acyclic: no cliques induced by directed cycles in O(D)" POSITION="bottom_or_right" ID="ID_1546988569" CREATED="1699357946728" MODIFIED="1699358014308"/>
<node TEXT="stable set S: induced subgraph with no connected vertices" POSITION="bottom_or_right" ID="ID_757366771" CREATED="1699358068966" MODIFIED="1699358156915"/>
<node TEXT="kernel K: absorbing stable set, i.e. all unconnected vertices" POSITION="bottom_or_right" ID="ID_213041677" CREATED="1699358159270" MODIFIED="1699358407838"/>
<node TEXT="holes" POSITION="bottom_or_right" ID="ID_677450703" CREATED="1699358558110" MODIFIED="1699367962767">
<node TEXT="odd hole: cycle of odd length &gt;= 5" POSITION="bottom_or_right" ID="ID_1455780868" CREATED="1699358516690" MODIFIED="1699367983852"/>
<node TEXT="odd antihole: complement" POSITION="bottom_or_right" ID="ID_528966613" CREATED="1699367972109" MODIFIED="1699525683403">
<arrowlink DESTINATION="ID_1495018257"/>
</node>
<node TEXT="filled: S(H) is odd (anti)hole" POSITION="bottom_or_right" ID="ID_1778889589" CREATED="1699368054259" MODIFIED="1699525831453">
<arrowlink DESTINATION="ID_816384941"/>
</node>
</node>
<node TEXT="\latex \overrightarrow{C_n} \text{: directed cycle}" POSITION="bottom_or_right" ID="ID_1747412145" CREATED="1699368105302" MODIFIED="1699368175098"/>
<node TEXT="D[V&apos;]: Notation for subdigraph" POSITION="bottom_or_right" ID="ID_592343268" CREATED="1699368203981" MODIFIED="1699368210077"/>
</node>
<node TEXT="3 A Strong Perfect Digraph Theorem" ID="ID_1085077822" CREATED="1699368327340" MODIFIED="1699368342873">
<node TEXT="D perfect &lt;=&gt; S(D) perfect, no directed cycles n&gt;=3 as induced subgraph" ID="ID_1345343871" CREATED="1699368344933" MODIFIED="1699525826057">
<arrowlink DESTINATION="ID_183352870"/>
</node>
<node TEXT="D perfect: feasible coloring of S(D) is feasible for D" ID="ID_1569891351" CREATED="1699368429087" MODIFIED="1699368454595"/>
<node TEXT="D perfect &lt;=&gt; no filled odd (anti)holes, no directed cycles n&gt;=3" ID="ID_323379395" CREATED="1699368454941" MODIFIED="1699526003850">
<arrowlink DESTINATION="ID_402006911"/>
</node>
</node>
<node TEXT="4 Some complexity results" ID="ID_141144044" CREATED="1699368929769" MODIFIED="1699368934495">
<node TEXT="maximum induced acyclic subdigraph of perfect digraph polynomial" ID="ID_1253193047" CREATED="1699368936534" MODIFIED="1699526020743"/>
<node TEXT="k-coloring of perfect digraphs polynomial" ID="ID_1355422119" CREATED="1699368995824" MODIFIED="1699526024925"/>
<node TEXT="recognition of perfect digraphs is co-NP-complete" ID="ID_1052112696" CREATED="1699369030133" MODIFIED="1699369037920"/>
</node>
<node TEXT="5 A Weak Perfect Digraph Theorem" ID="ID_351935963" CREATED="1699369045850" MODIFIED="1699369054709">
<node TEXT="no analogon for undirected Weak Perfect Digraph Theorem" ID="ID_1113157964" CREATED="1699369054931" MODIFIED="1699369105262">
<node TEXT="complement of perfect digraph may be not perfect" ID="ID_331374842" CREATED="1699369109908" MODIFIED="1699369120034">
<node TEXT="C4 not perfect but complement is" ID="ID_1800089246" CREATED="1699369132000" MODIFIED="1699369141567"/>
</node>
</node>
<node TEXT="**Theorem 9**: digraph perfect &lt;=&gt; complement is clique-acyclic superorientation of perfect graph" ID="ID_1481120539" CREATED="1699369105787" MODIFIED="1699369651619"/>
<node TEXT="**Lemma 10**: equivalent for digraph D:" ID="ID_827230266" CREATED="1699375203925" MODIFIED="1699375518507">
<node TEXT="D no induced directed cycle" ID="ID_1524705621" CREATED="1699375263350" MODIFIED="1699375319571"/>
<node TEXT="complement(D) has no induced complements of directed cycles" ID="ID_447293334" CREATED="1699375270606" MODIFIED="1699375489295"/>
<node TEXT="complement(D) is clique-acyclic" ID="ID_396679331" CREATED="1699375284591" MODIFIED="1699375495495"/>
</node>
<node TEXT="**Corollary 11**: Recognition of clique-acyclic superorientations of perfect graphs is co-NP-complete" ID="ID_482129598" CREATED="1699375519396" MODIFIED="1699375581571"/>
<node TEXT="**Theorem 12**: Perfect graphs are kernel-solvable" ID="ID_1013952985" CREATED="1699375657086" MODIFIED="1699375672026">
<node TEXT="every clique-acyclic superorientation of a perfect graph has a kernel" ID="ID_1964431436" CREATED="1699375672033" MODIFIED="1699375696692"/>
</node>
<node TEXT="**Corollary 13**: complements of perfect digraphs have kernel" ID="ID_1653850459" CREATED="1699375721778" MODIFIED="1699375767372"/>
<node TEXT="**Theorem 14**: kernel-presence decision is NP-complete" ID="ID_110634997" CREATED="1699375764026" MODIFIED="1699375786467"/>
</node>
<node TEXT="6 Open questions" ID="ID_1429618527" CREATED="1699375806704" MODIFIED="1699375812038">
<node TEXT="**Open question 15**: special classes with efficient algorithms for other problems than coloring / maximum acyclic subdigraph?" ID="ID_1492572851" CREATED="1699375812331" MODIFIED="1699375884677"/>
<node TEXT="**Open question 16**: other (co-)NP-complete problems for (perfect) digraphs?" ID="ID_956701869" CREATED="1699375885089" MODIFIED="1699375949476"/>
<node TEXT="**Open question 17**: Complexity of recognizing superorientations of perfect graphs with kernel" ID="ID_151284272" CREATED="1699375949645" MODIFIED="1699376031085"/>
</node>
<node TEXT="Appendix" ID="ID_882531110" CREATED="1699376047408" MODIFIED="1699376049204">
<node TEXT="**Theorem 18**: Deciding kernel-perfectness of a digraph is co-NP-hard" ID="ID_52270576" CREATED="1699376049210" MODIFIED="1699376069218"/>
</node>
</node>
<node TEXT="Andres et al., 2020: A semi-strong perfect digraph theorem" POSITION="bottom_or_right" ID="ID_244874649" CREATED="1699377080462" MODIFIED="1699377147165">
<node TEXT="1. Introduction" ID="ID_1160944503" CREATED="1699377105350" MODIFIED="1699448439302">
<node TEXT="clique number == dichromatic number" ID="ID_1455214053" CREATED="1699377213778" MODIFIED="1699377350475"/>
<node TEXT="undirected: P4-isomorphic =&gt; either both or none are perfect (Reed)" ID="ID_1523849457" CREATED="1699377350870" MODIFIED="1699377382368"/>
<node TEXT="only digraphs without loops!" ID="ID_1576029451" CREATED="1699377370079" MODIFIED="1699377404598"/>
<node TEXT="symmetric part" ID="ID_464343844" CREATED="1699377404990" MODIFIED="1699377411272"/>
<node TEXT="k-coloring: c: V -&gt; {1,...,k}, induced subgraph for all colors is acyclic" ID="ID_911260282" CREATED="1699377411602" MODIFIED="1699445100616">
<node TEXT="dichromatic number X(D): smallest k" POSITION="bottom_or_right" ID="ID_910863886" CREATED="1699445119625" MODIFIED="1699445400923"/>
</node>
<node TEXT="clique: fully bidirectional connected subdigraph" ID="ID_1043111607" CREATED="1699445177183" MODIFIED="1699445205113">
<node TEXT="clique number w(D): size of largest clique in S(D)" POSITION="bottom_or_right" ID="ID_1890735303" CREATED="1699445221666" MODIFIED="1699445411466">
<node TEXT="O(D) is clique-free!" ID="ID_1432730920" CREATED="1699445263704" MODIFIED="1699445274220"/>
<node TEXT="lower bound for dichromatic number" ID="ID_1069948787" CREATED="1699445322363" MODIFIED="1699445327454">
<node TEXT="perfect if equal" ID="ID_185213882" CREATED="1699445371744" MODIFIED="1699445373947"/>
</node>
</node>
</node>
<node TEXT="symmetric digraph: replace undirected edges by bidirectional arcs" ID="ID_1031647010" CREATED="1699445233381" MODIFIED="1699445451036">
<node TEXT="dichromatic number == chromatic number" ID="ID_1012676768" CREATED="1699445488049" MODIFIED="1699445511554"/>
<node TEXT="clique number == clique number" ID="ID_815835783" CREATED="1699445511900" MODIFIED="1699445522483"/>
<node TEXT="perfect iff perfect" ID="ID_1864378422" CREATED="1699445522645" MODIFIED="1699445528699"/>
</node>
<node TEXT="**Theorem 1**: perfect &lt;=&gt; S(D) perfect, no directed cycle &gt;= 3" ID="ID_1057975890" CREATED="1699445541030" MODIFIED="1699445668517">
<node TEXT="Strong Perfect Graph Theorem: forbidden induced subgraphs" ID="ID_444180218" CREATED="1699445700226" MODIFIED="1699445711623"/>
</node>
<node TEXT="Weak Perfect Graph Theorem does not generalize" ID="ID_1320510076" CREATED="1699445689976" MODIFIED="1699445738124">
<node TEXT="C4 is perfect, complement is not" ID="ID_37025295" CREATED="1699445738132" MODIFIED="1699445802596"/>
<node TEXT="perfection not maintained under complements" ID="ID_228180584" CREATED="1699445802788" MODIFIED="1699445810535"/>
</node>
<node TEXT="P4-isomorphic: any 4 vertices P4 in G &lt;=&gt; P4 in H" ID="ID_1255942033" CREATED="1699445881394" MODIFIED="1699445952703"/>
<node TEXT="**Theorem 2** Semi-strong Perfect (undirected) Graph Theorem: P4 isomorphic =&gt; G perfect &lt;=&gt; H perfect" ID="ID_651604453" CREATED="1699445964811" MODIFIED="1699446227105"/>
<node TEXT="cographs: no induced P4" ID="ID_1115538696" CREATED="1699446061422" MODIFIED="1699446065252">
<node TEXT="any pair of cographs with equal |V| is P4-isomorphic" ID="ID_1073411118" CREATED="1699446065257" MODIFIED="1699446080067"/>
</node>
<node TEXT="directed cographs: 8 forbidden minors (set F)" ID="ID_946313870" CREATED="1699446215059" MODIFIED="1699446339350">
<node TEXT="class is invariant under complements" ID="ID_1178828450" CREATED="1699446265686" MODIFIED="1699446300079">
<node TEXT="isomorphism w.r.t. F not the right notion" ID="ID_1902841699" CREATED="1699446288080" MODIFIED="1699446366123">
<node TEXT="TODO: Why?" ID="ID_1247947443" CREATED="1699446411788" MODIFIED="1699446414253"/>
</node>
<node TEXT="restrict F to 5 minors" ID="ID_1151376928" CREATED="1699446366606" MODIFIED="1699446376042"/>
</node>
</node>
</node>
<node TEXT="2. P4C-isomorphic digraphs" ID="ID_481507481" CREATED="1699446426512" MODIFIED="1699448434587">
<node TEXT="5 forbidden minors" ID="ID_981436046" CREATED="1699446431408" MODIFIED="1699446443847">
<node TEXT="symmetric P4" ID="ID_60745097" CREATED="1699446443852" MODIFIED="1699446447470">
<node TEXT="png-231108-135727314-1430766121742615749.png" ID="ID_1348145629" CREATED="1699448253390" MODIFIED="1699448253390">
<hook URI="PerfectDigraphs_files/png-231108-135727314-1430766121742615749.png" SIZE="0.8480646" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="directed C3" ID="ID_1970150066" CREATED="1699446447819" MODIFIED="1699446458192">
<node TEXT="png-231108-135809881-15771970369432149996.png" ID="ID_867954620" CREATED="1699448292154" MODIFIED="1699448292154">
<hook URI="PerfectDigraphs_files/png-231108-135809881-15771970369432149996.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="directed P3" ID="ID_635835435" CREATED="1699446451445" MODIFIED="1699446461771">
<node TEXT="png-231108-135821934-17130295690078040613.png" ID="ID_200411459" CREATED="1699448303115" MODIFIED="1699448303115">
<hook URI="PerfectDigraphs_files/png-231108-135821934-17130295690078040613.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
<node TEXT="directed P3+ and P3- (one antiparallel edge)" ID="ID_1765426575" CREATED="1699446462296" MODIFIED="1699446520966">
<node TEXT="png-231108-135932255-17293189347182634135.png" ID="ID_1253072290" CREATED="1699448373323" MODIFIED="1699448373323">
<hook URI="PerfectDigraphs_files/png-231108-135932255-17293189347182634135.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
<node TEXT="png-231108-135838163-16932638463926335130.png" POSITION="bottom_or_right" ID="ID_63650834" CREATED="1699448319216" MODIFIED="1699448319216">
<hook URI="PerfectDigraphs_files/png-231108-135838163-16932638463926335130.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
<node TEXT="**Definition 3**: D and D&apos; P4C-isomorphic on same vertex set, iff" ID="ID_1742898724" CREATED="1699446540832" MODIFIED="1699446597723">
<node TEXT="any 4 nodes a P4 in S(D) iff also in S(D&apos;)" ID="ID_738589722" CREATED="1699446585049" MODIFIED="1699447695069"/>
<node TEXT="any 3 nodes a directed C3 in D iff also in D&apos;" ID="ID_1483606164" CREATED="1699447567528" MODIFIED="1699447687797"/>
<node TEXT="" ID="ID_622870002" CREATED="1699447820924" MODIFIED="1699447820924">
<hook NAME="FirstGroupNode"/>
</node>
<node TEXT="any 3 nodes a directed P3 with midpoint b in D iff also in D&apos;" ID="ID_1000463386" CREATED="1699447696594" MODIFIED="1699447813484"/>
<node TEXT="any 3 nodes a directed P3+ or P3- with midpoint b iff also in D&apos;" ID="ID_808436954" CREATED="1699447776850" MODIFIED="1699447806876"/>
<node TEXT="" ID="ID_1899962907" CREATED="1699447820921" MODIFIED="1699447820923">
<hook NAME="SummaryNode"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="TODO: why midpoint?" ID="ID_3064387" CREATED="1699447771275" MODIFIED="1699447775420"/>
</node>
</node>
<node TEXT="**Lemma 4**: D + D&apos; P4C-isomorphic &lt;=&gt; cycle &gt;= 3 in D iff in D&apos;" ID="ID_141280068" CREATED="1699447916246" MODIFIED="1699448004429"/>
<node TEXT="**Theorem 5**: D and D&apos; P4C isomorphic: D perfect &lt;=&gt; D&apos; perfect" ID="ID_801148604" CREATED="1699448045044" MODIFIED="1699448113290"/>
</node>
<node TEXT="3. Transitive exensions of cographs" ID="ID_1794179449" CREATED="1699448425466" MODIFIED="1699448430943">
<node TEXT="without 5 forbidden subgraphs -&gt; pairwise P4C-isomorphic" ID="ID_1089797752" CREATED="1699479013443" MODIFIED="1699479065884"/>
<node TEXT="sym(D) cograph -&gt; consider cotree" ID="ID_1546389669" CREATED="1699479066264" MODIFIED="1699479085263">
<node TEXT="canonical form: labels alternate 0/1" ID="ID_1336755512" CREATED="1699479085279" MODIFIED="1699479091280"/>
<node TEXT="1-labeled tree vertices: complete joins" ID="ID_579331515" CREATED="1699479139957" MODIFIED="1699479156130">
<node TEXT="no additional asymmetric arcs" ID="ID_877521912" CREATED="1699479156137" MODIFIED="1699479162651"/>
</node>
<node TEXT="0-labeled: disjoint unions" ID="ID_1742193685" CREATED="1699479164260" MODIFIED="1699479197696"/>
</node>
<node TEXT="connected components in S(G): G1,...,Gk" ID="ID_1814034508" CREATED="1699479200465" MODIFIED="1699479216342"/>
<node TEXT="**Lemma 6**: asymmetric arc from Gi to Gj: Gi and Gj connected by orientation complete bipartite graph K_V(Gi),V(Gj)" ID="ID_1777480559" CREATED="1699479223039" MODIFIED="1699479409911">
<node TEXT="G1,...,Gk: asymmetric arcs constitute orientation of complete l-partite graph, l &lt; k" ID="ID_467664353" CREATED="1699479562361" MODIFIED="1699479597202"/>
<node TEXT="must not contain directed C3 or directed P3" ID="ID_1625493524" CREATED="1699479647251" MODIFIED="1699479736166"/>
<node TEXT="asymmetric arcs in Gi possible" ID="ID_1613359274" CREATED="1699479736501" MODIFIED="1699479743136"/>
</node>
<node TEXT="Is this structure strict enough to make some problems possible that are NP-complete" ID="ID_1316038601" CREATED="1699479744519" MODIFIED="1699479775446">
<node TEXT="complexity of vertex coverage with minimum disjoint directed paths" ID="ID_915175595" CREATED="1699479776718" MODIFIED="1699479855566"/>
</node>
</node>
</node>
</node>
</map>
