# Perfect Digraphs / Gerichtete perfekte Graphen

## Aus den Einführungsfolien
### Inhalte des Vortrags
- Definition perfekter gerichteter Graphen
- Strong Perfect Digraph Theorem (ohne Beweis)
- Semi-Strong Perfect Digraph Theorem (ohne Beweis)
- Nicht perfekte Graphklassen definieren und begründen
	- planare / bipartite gerichtete Graphen, Turniergraphen

### Literatur:
- Bang-Jensen, J. and Gutin, G., editors (2018). *Classes of Directed Graphs*, chapter 11.7 Springer-Verlag, Berlin.
- Andres and Hochstättler, 2015. Perfect digraphs. *Journal of Graph Theory*, 79(1):21-29.
- Andres, S., Bergold, H., Hochstättler, W., and Wiehe, J. (2020). A semi-strong perfect digraph theorem. *AKCE International Journal of Graphs and Combinatorics, 17(3):992–994.*

### Übungsaufgaben
- Geben Sie alle gerichteten perfekten Graphen mit höchstens 3 Knoten an.
- Welche der acht gerichteten Graphen D1, ..., D8 auf Folie 30 sind perfekt?
- Zeigen oder widerlegen Sie: Jeder biorientierte Kreis Cn ist ein perfekter gerichteter Graph.
- Zeigen Sie: Für jeden perfekten gerichteten Graphen G ist jede azyklische k-Färbung von sym(G) auch eine azyklische k-Färbung von G.
- Wie kann man in einem gerichteten Graphen G die Knotenanzahl eines größten induzieren azylischen Teilgraphen mit Hilfe von sym(G) bestimmen?

## Notes
- *perfect undirected graph*: chromatic number = clique number for every induced subgraph
	- complete biorientation (undirected edge -> pair of opposing directed edges) is perfect
- NP-complete problems get polynomial
	- e.g. determining chromatic, clique or independence number
- applicable in practice: common graph classes are perfect
	- e.g. bipartite, chordal, triangulated, interval, comparability graphs
- *Strong Perfect Graph Conjecture* proven after 40 years -> theorem
	- perfect <=> no odd holes / antiholes as induced subgraphs
		- **odd hole**: induced cycle of odd length >= 5
		- odd antihole: complement
	- graphs without odd holes + antiholes: recognized in polynomial time
- Andres & Hochstättler: perfect digraph class, *Strong Perfect Digraph Theorem*
	- dichromatic number instead of chromatic number for digraphs
- **dichromatic** number: smallest number of colors k s.t. vertex coloring has no monochromatic directed cycles
	- TODO: Difference to chromatic number for digraphs?
- **clique** number: order of largest complete subdigraph
- perfect directed graph: dichromatic number == clique number for any induced subgraph
	- TODO: Just "any" or "every"?
	- natural extension of perfectness to digraphs
- **symmetric part** sym(D) = spanning subdigraph containing the symmetric arcs
	- asymmetric part analogous
- **Strong Perfect Digraph Theorem 11.7.1**: D perfect <=>
	- sym(D) perfect
	- no directed cycles >= 3 as induced subdigraph
- **filled odd hole**: sym(D) is *complete biorientation* of odd hole
	- filled odd *anti*hole analogous
- **corollary 11.7.2**: perfect <=> no filled odd holes/antiholes, no cycles >= 3 as induced subdigraphs
	- using Strong Perfect *Graph* Theorem
- perfect digraph: *symmetric part* determines validity of k-dicolouring
	- **corollary 11.7.3**: perfect digraph: k-dicolouring of sym(D) => k-dicolouring of D
- maximum order of induced acyclic subdigraph depends solely on sym(D)
	- **corollary 11.7.4**: determining chromatic & clique number and maximum order of induced acyclic subdigraph is polynomial
- **problem 11.7.5** other NP-complete problems that get polynomial for perfect digraphs?
- **problem 11.7.10** NP-(co)-complete problems that remain?
- perfect digraph share many favourable properties of perfect graphs, but
	- **theorem 11.7.6**: deciding if digraph is perfect is co-NP-complete
	- **theorem 11.7.7**: deciding if perfect digraph has kernel is NP-complete
- **corollary 11.7.8**: complement of perfect digraph is kernel-perfect (every induced subdigraph has kernel)
- 11.7.7 + 11.7.8 => complements of perfect digraphs not necessarily perfect (Figure 11.8(a) + (d))
	- undirected: *Weak Perfect Graph Theorem*
- **theorem 11.7.9**: D perfect <=> complement(D) is biorientation of perfect G, no induced cliques in G by cycles in asym(complement(D))
- for undirected G: complement(biorientation(G)) = complement(G)
	- 11.7.9 generalization of *Weak Perfect Graph Theorem*
- **maker-breaker game**: players take turns colouring vertices
	- no two adjacent vertices same colour
	- stop if properly coloured or no next proper colouring possible
	- **game chromatic number** Xg(G) smallest number of colours with winning strategy, max |V(G)|
	- *Andres for digraphs*: inbound vertices only
		- natural definition, as Xg(G) = Xg(biorientation(G))
	- *Yang and Zhu*: avoid monochromatic cycles
		- **weak game chromatic number** / **game dichromatic number**
	- **game-perfect** <=> game chromatic number = clique number
	- **weakly game-perfect** analogously
	- game-perfect digraphs proper subclass
- **theorem 11.7.11**: game-perfect => kernel-perfect
- **problem 11.7.12**: set of forbidden induced subdigraphs for game-perfect digraphs
- **theorem 11.7.13**: D weakly game-perfect <=> undirected sym(D) game-perfect and no directed cycles >= 3
	- **corollary 11.7.14**: D weakly game-perfect <=> no directed cycle >=3 and no C4, P4, triangle star, E-graph, two double-fans, two split 3-stars
