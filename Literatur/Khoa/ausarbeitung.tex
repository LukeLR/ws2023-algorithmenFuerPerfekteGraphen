\documentclass{article}
\usepackage{amsfonts,amssymb,amsmath,epsfig,rotating,amssymb,framed,enumerate,enumitem,array,mathrsfs}

\begin{document}
\title{Defining perfect digraphs based on variations of coloring and cliques in digraphs}
\author{Nguyen Khoa Tran}
\maketitle

\begin{abstract}
In this paper we consider possibilities of defining a perfect digraph which are obtained by combining different variations of coloring and cliques in digraphs. For each possible perfect digraph, we analyze whether it is bounded in terms of the following digraph parameters: directed path-width, directed tree-width, and directed clique-width.
\end{abstract}

\section{Introduction}
Undirected perfect graphs are of interest in graph theory because the graph coloring problem, maximum clique problem, and so forth can be solved in polynomial time, as stated in \cite{CCL05}. To facilitate further investigation on this topic for digraphs, we provide possible definitions of perfect digraphs and analyze their bounds regarding three digraph parameters.

In section \ref{second-section} we introduce some basic definitions.

Section \ref{third-section} of this paper deals with defining different possibilities of perfect digraphs based on variations of coloring and cliques in digraphs.

In section \ref{fourth-section} we define the digraph parameters directed path-width, directed tree-width, and directed clique-width, and analyze the above mentioned possibilities of perfect digraphs in terms of these parameters.

Eventually, the results of this paper are summarized in a table and an outlook is given.

\section{Preliminaries}\label{second-section}
We use the notations of \cite{GR18} for graphs and digraphs.

Furthermore, we assume that there are neither self-loops nor multi-edges nor multi-arcs.

Whenever we work with a number $n$, we assume that $n \in \mathbb{N} \setminus \{0\}$ holds true.

\subsection{Graphs}
A \textit{graph} is a pair $G = (V, E)$, where $V$ is a non-empty, finite set of \textit{vertices} and $E \subseteq \{\{u, v\} \ | \ u, v \in V, u \neq v\}$ is a finite set of \textit{edges}. A graph $G' = (V', E')$ is a \textit{subgraph} of graph $G = (V, E)$ if $V' \subseteq V$ and $E' \subseteq E$. If every edge of $E$ with both end vertices in $V'$ is in $E'$, we say that $G'$ is an \textit{induced subgraph} of graph $G$ and we write $G' = G[V']$. %For some undirected graph $G = (V, E)$ its \textit{complement graph} is defined by $\overline{G} = (V, \{\{u, v\} \ | \ \{u, v\} \notin E, u, v \in V, u \neq v\})$.

\paragraph{Definition 2.1 (perfect graphs, \cite{BLS99})} A subset $V' \subseteq V$ is a \textit{clique} in a graph $G$ if for all $u, v \in V', u \neq v, \{u, v\} \in E$.
\newline
The \textit{clique number} of a graph $G$ is $\omega(G) = \max \{|V'| \ | \ V' \subseteq V$ and $V'$ is a clique in $G\}$.

The \textit{chromatic number} $\chi(G)$ of a graph $G = (V, E)$ is the lowest number of colors needed for coloring all vertices of $G$ such that no two adjacent vertices have the same color. Two vertices $u, v \in V$ are adjacent if there exists an edge $\{u, v\} \in E$.

A graph $G$ is perfect if for all induced subgraphs $H$ of $G$, $\chi(H) = \omega(H)$.

\subsection{Digraphs}
A \textit{directed graph} or \textit{digraph} is a pair $G = (V, E)$, where $V$ is a non-empty, finite set of \textit{vertices} and $E \subseteq \{(u, v) \ | \ u, v \in V, u \neq v\}$ is a finite set of ordered pairs of distinct vertices called \textit{arcs}. If $(u, v) \in E$, then $v$ is the \textit{head} of the arc and $u$ is its \textit{tail}. An \textit{indegree} of a vertex, $indeg(v)$ with $v \in V$, is the number of arcs where $v$ is the head. An \textit{outdegree} of a vertex, $outdeg(v)$ with $v \in V$, is the number of arcs where $v$ is the tail. A digraph $G' = (V', E')$ is a \textit{subdigraph} of digraph $G = (V, E)$ if $V' \subseteq V$ and $E' \subseteq E$. If every arc of $E$ with both end vertices in $V'$ is in $E'$, we say that $G'$ is an \textit{induced subdigraph} of digraph $G$ and we write $G' = G[V']$. Two digraphs $F = (V_F, E_F)$ and $H = (V_H, E_H)$ are \textit{vertex-disjoint} if $V_F \cap V_H = \emptyset$.%For some digraph $G = (V, E)$ its \textit{complement digraph} is defined by $\overline{G} = (V, \{(u, v) \ | \ (u, v) \notin E, u, v \in V, u \neq v\})$ and its \textit{converse digraph} is defined by $G^c = (V, \{(u, v) \ | \ (v, u) \in E, u, v \in V, u \neq v\})$.

\paragraph{Definition 2.2 (directed cycles)} A \textit{directed cycle} is a digraph such that every vertex has an in- and outdegree of 1. The \textit{length} of a directed cycle is the number of vertices in it.
\newline
\newline
An \textit{acyclic} digraph is a digraph without any directed cycles as a subdigraph. 

\section{Perfect digraphs}\label{third-section}
In this section we introduce variations of coloring and cliques in digraphs, followed by possible definitions of perfect digraphs through combinations of one variation of coloring and clique each.

\subsection{Coloring in digraphs}
This subsection names two coloring variations in digraphs. The first coloring variation follows the same concept as the one for undirected graphs, whereas the second coloring variation also takes the orientations of the arcs into consideration. Note that a $k$-\textit{coloring} is a coloring using $k$ colors.

\paragraph{Definition 3.1 (normal coloring)} A $k$-coloring $c_1$ of a digraph $G = (V, E)$ is \textit{normal} if for all $(u, v) \in E$ and $u, v \in V$ it holds that $c_1(u) \neq c_1(v)$. That is, a normal $k$-coloring $c_1$ of $G$ is canonical if no two adjacent vertices of $und(G)$ (see Definition 3.6) have the same color.

We define the \textit{chromatic number for normal coloring} in digraphs as follows: The chromatic number $\chi_1(G)$ of a digraph $G$ is the lowest number of colors $k$ needed for a normal coloring of all vertices.

\paragraph{Definition 3.2 (good coloring, \cite{Cou94})} A $k$-coloring $c_2$ of a digraph $G = (V, E)$ is \textit{good} if, whenever there is an arc linking $u$ to $v$ with $u, v \in V$, there is no arc linking a vertex with color $c_2(v)$ to a vertex with color $c_2(u)$.

We define the \textit{chromatic number for good coloring} in digraphs as follows: The chromatic number $\chi_2(G)$ of a digraph $G$ is the lowest number of colors $k$ needed for a good coloring of all vertices.

\subsection{Cliques in digraphs}
We consider three variations of directed cliques in digraphs. Each of them has been selected such that for both coloring variations the number of vertices in the directed clique equals the chromatic number of it.

\paragraph{Definition 3.4 (transitive tournaments as directed cliques)} Let $G = (V, E)$ be a digraph. An induced subdigraph $\overrightarrow{T_n} = (V', E'),$ $n = |V'|$, is a \textit{transitive tournament} if for all $v_i, v_j \in V'$, it holds that $(v_i, v_j) \in E'$ with $i < j$. Transitive tournaments are acyclic by definition.

We define $\overrightarrow{\omega_A}(G) = \max \{n \ | \ \overrightarrow{T_n}$ is a transitive tournament in $G\}$ as the \textit{directed clique number} of digraph $G$.

\paragraph{Definition 3.5 (complete digraphs as directed cliques)} Let $G = (V, E)$ be a digraph. An induced subdigraph $\overleftrightarrow{K_n} = (V', E'),$ $n = |V'|$, is \textit{complete} if for all $u, v \in V', u \neq v$, it holds that $(u, v), (v, u) \in E$.

We define $\overrightarrow{\omega_B}(G) = \max \{n \ | \ \overleftrightarrow{K_n}$ is complete in $G\}$ as the \textit{directed clique number} of digraph $G$.

\paragraph{Definition 3.6 (underlying graphs as directed cliques)} Let $G = (V, E)$ be a digraph. The \textit{underlying graph} $und(G) = (V, E')$ of $G$ is an undirected graph such that $E' = \{\{u, v\} \ | \ (u, v) \in E, u, v \in V, u \neq v)\}$.

We define $\overrightarrow{\omega_C}(G) = \omega(und(G))$ as the \textit{directed clique number} of digraph $G$.

\subsection{Definitions of perfect digraphs}
Now we have six different possibilities for defining perfect digraphs: transitive tournaments with normal coloring (A1 for short), transitive tournaments with good coloring (A2 for short), complete digraphs with normal coloring (B1 for short), complete digraphs with good coloring (B2 for short), underlying graphs with normal coloring (C1 for short), and underlying graphs with good coloring (C2 for short).

From now on, we denote perfect digraphs in the graph class of A1, A2, B1, B2, C1, C2 as A1-, A2-, B1-, B2-, C1-, C2-digraphs, respectively.

\paragraph{Definition 3.7 (perfect digraph)} We define our six different possibilities of perfect digraphs as follows:
\newline
A1: A digraph $G$ is \textit{perfect} if for all induced subdigraphs $H$ of $G$, $\chi_1(H)$=$\overrightarrow{\omega_A}(H)$.
\newline
A2: A digraph $G$ is \textit{perfect} if for all induced subdigraphs $H$ of $G$, $\chi_2(H)$=$\overrightarrow{\omega_A}(H)$.
\newline
B1: A digraph $G$ is \textit{perfect} if for all induced subdigraphs $H$ of $G$, $\chi_1(H)$=$\overrightarrow{\omega_B}(H)$.
\newline
B2: A digraph $G$ is \textit{perfect} if for all induced subdigraphs $H$ of $G$, $\chi_2(H)$=$\overrightarrow{\omega_B}(H)$.
\newline
C1: A digraph $G$ is \textit{perfect} if for all induced subdigraphs $H$ of $G$, $\chi_1(H)$=$\overrightarrow{\omega_C}(H)$.
\newline
C2: A digraph $G$ is \textit{perfect} if for all induced subdigraphs $H$ of $G$, $\chi_2(H)$=$\overrightarrow{\omega_C}(H)$.

\paragraph{Lemma 3.8} The graph classes of A1 and B1 are strict subsets of the graph class of C1.

\paragraph{Proof} Underlying graphs of every transitive tournament and every complete digraph are cliques by definition. By implication the graph class of A1 is a subset of the graph class of C1 because the former does not define complete digraphs as perfect digraphs, whereas the latter does. Analogously the graph class of B1 is a subset of the graph class of C1 because the former does not define transitive tournaments as perfect digraphs, whereas the latter does. $\hfill \square$

\paragraph{Corollary 3.9} A good coloring for complete digraphs only works for digraphs with no arcs.
\newline
\newline
We exclude B2 from our definition and analysis because we do not think that it will lead to meaningful results.

\paragraph{Lemma 3.10} For every perfect digraph $G$ with a transitive tournament $\overrightarrow{T_n}$ with $n$ being the directed clique number of $G$, good coloring is impossible if there is a directed cycle as a subdigraph.

\paragraph{Proof} Let $G = (V, E)$ be a perfect digraph with $\chi_2(G) = n$ and let $\overrightarrow{T_n} = (V_T, E_T)$, $n = |V_T|$, be a transitive tournament. Furthermore, we label our colors as numbers from $1$ to $n$ such that for $u \in V_T$, $c_2(u) = indeg(u) + 1$. For every arc $(u_T, v_T) \in E_T$, $indeg(u_T) < indeg(v_T)$. Hence, for all $(u, v) \in E$ it holds that, if $c_2(u) = c_2(u_T)$ and $c_2(v) = c_2(v_T)$, then $c_2(u) < c_2(v)$.

Let us now assume that we have a directed cycle $\overrightarrow{C_m} = (V_C, E_C)$ with $V_C = \{v_1, \dots, v_m\}$ as a subdigraph of $G$. Since $\overrightarrow{C_m}$ is a directed cycle, without loss of generality $E_C = \{(v_1, v_2), (v_2, v_3), \dots, (v_{m-1}, v_m), (v_m, v_1)\}$. From this it follows that $c_2(v_1) < c_2(v_2) < \dots < c_2(v_m) < c_2(v_1)$, which implies the contradiction $c_2(v_1) < c_2(v_1)$. Thus, the assumption must be wrong and we have shown that Lemma 3.10 holds true.$\hfill \square$
\newline
\newline
Following Lemma 3.10, we know that all A2-digraphs are acyclic.

\paragraph{Conjecture 3.11} $\{G \ | \ G$ is an A2-digraph$\} \cup \{G \ | \ G$ contains at least one directed cycle of length $n$ as a subdigraph and all directed cycles have length $m$ with $3 \leq n, m \leq \overrightarrow{\omega_C}(G)\} = \{G \ | \ G$ is a C2-digraph$\}.$
\newline
\newline
We could not prove Conjecture 3.11, but the reasoning is shown below:
\newline
Every A2-digraph is a C2-digraph by definition. Moreover, a C2-digraph can have the following form: Let $\overrightarrow{C_n} = (V', E')$ be a directed cycle with $n \geq 3$ and let $G = (V, E)$ be a C2-digraph, $V' \subseteq V$ and $E' \subseteq E$. If $(u, v), (v, u) \notin E'$ for all $u, v \in V'$, either of them is in $E$.
\newline
If there were another directed cycle $\overrightarrow{C_m} = (V_C, E_C)$ as a subdigraph in $G$ with $m > \overrightarrow{\omega_C}(G)$, $G$ would not be a C2-digraph because we would need more than $\overrightarrow{\omega_C}(G)$ colors, resulting in $\overrightarrow{\omega_C}(G) \neq \chi_2(G)$ (without proof).

\section{Analysis of digraph parameters}\label{fourth-section}
First of all, we define the necessary digraph parameters. After that, we analyze the upper and lower bounds of said parameters for each definition of perfect digraphs.

\paragraph{Definition 4.1 (directed path-width, \cite{GR18})} A \textit{directed path-decomposition} of a digraph $G = (V, E)$ is a sequence $(X_1, \dots, X_r)$ of subsets of $V$ such that the following three conditions hold true:
\begin{enumerate}[label=(\roman*)]
	\item $\bigcup^{r}_{i=1} X_i = V$.
	\item For each $(u, v) \in E$ there is a pair $i \leq j$ such that $u \in X_i$ and $v \in X_j$.
	\item If $u \in X_i$ and $u \in X_j$ for some $u \in V$ and two indices $i,j$ with $i \leq j$, then $u \in X_l$ for all indices $l$ with $i \leq l \leq j$.
\end{enumerate}
The \textit{width} of a directed path-decomposition is $\max_{1 \leq i \leq r} \{|X_i| - 1\}$. The \textit{directed path-width} of $G$, $dpw(G)$ for short, is the smallest integer $w$ such that there is a directed path-decomposition of $G$ of width $w$.

\paragraph{Corollary 4.2 (\cite{YC08})} For an acyclic graph $G$, $dpw(G) = 0$.

\paragraph{Corollary 4.3 (\cite{GRR18})} For a complete digraph $\overleftrightarrow{K_n}$, $dpw(\overleftrightarrow{K_n}) = n - 1$.

\paragraph{Lemma 4.4 (\cite{GR18})} Let $F = (V_F, E_F)$ and $H = (V_H, E_H)$ be two vertex-disjoint digraph and let $G = (V_F \cup V_H, E_F \cup E_H)$, then $dpw(G) = \max\{dpw(F), dpw(H)\}$.

\paragraph{Definition 4.5 (directed tree-width, \cite{GR18})} An \textit{out-tree} is a digraph with a distinguished root such that all arcs are directed away from the root. For two vertices $u, v$ of an out-tree $T$ the notation $u \leq v$ means that there is a directed path on $\geq 0$ arcs from $u$ to $v$ and $u < v$ means that there is a directed path on $\geq 1$ arcs from $u$ to $v$.

Let $G = (V, E)$ be some digraph and $Z \subseteq V$. A vertex set $S \subseteq V$ is $Z$\textit{-normal}, if there is no directed walk in $G - Z$ with first and last vertices in $S$ that uses a vertex of $G - (Z \cup S)$.

A \textit{tree-decomposition} of a digraph $G = (V_G, E_G)$ is a triple $(T, X, W)$. Here $T = (V_T, E_T)$ is an out-tree, $X = \{X_e \ | \ e \in E_T\}$ and $W = \{W_r \ | \ r \in V_T\}$ are sets of subsets of $V_G$, such that the following two conditions hold true:
\begin{enumerate}[label=(\roman*)]
	\item $W = \{W_r \ | \ r \in V_T\}$ is a partition of $V_G$ into non-empty subsets.
	\item For every $(u, v) \in E_T$ the set $\bigcup \{W_r \ | \ r \in V_T, v \leq r\}$ is $X_{(u, v)}$-normal.
\end{enumerate}
The \textit{width} of a tree-decomposition $(T, X, W)$ is $\max_{r \in V_T} \{|W_r \cup \bigcup_{e \sim r} X_e| - 1\}$. Here $e \sim r$ means that $r$ is one of the two vertices of arc $e$. The \textit{directed tree-width} of $G$, $dtw(G)$ for short, is the smallest integer $k$ such that there is a tree-decomposition $(T, X, W)$ of $G$ of width $k$.

\paragraph{Corollary 4.6 (\cite{JRST01})} For an acyclic digraph $G$, $dtw(G) = 0$.

\paragraph{Corollary 4.7 (\cite{GR18})} For a complete digraph $\overleftrightarrow{K_n}$, $dtw(\overleftrightarrow{K_n}) = n - 1$.

\paragraph{Lemma 4.8 (\cite{GR18})} Let $F = (V_F, E_F)$ and $H = (V_H, E_H)$ be two vertex-disjoint digraphs and let $G = (V_F \cup V_H, E_F \cup E_H)$, then $dtw(G) = \max\{dtw(F), dtw(H)\}$.

\paragraph{Definition 4.9 (directed clique-width, \cite{GWY16})} Let $k$ be some positive integer. The class $dCW_k$ of labeled digraphs is recursively defined as follows.
\begin{enumerate}
	\item The single vertex graph $\bullet_{a}$ for some $a \in [k]$ is in $dCW_k$ where $[k] = \{1,...,k\}$.
	\item Let $G = (V_G, E_G, lab_G) \in dCW_k$ and $J = (V_J, E_J, lab_J) \in dCW_k$ be two vertex-disjoint labeled digraphs, then $G \oplus J := (V', E', lab')$ defined by $V' := V_G \cup V_J$, $E' := E_G \cup E_J$, and
	\newline
	$lab'(u) :=
	\begin{cases}
		lab_G(u) & \text{if } u \in V_G\\
		lab_J(u) & \text{if } u \in V_J
	\end{cases}$
	\newline
	for every $u \in V'$ is in $dCW_k$.
	\item Let $a, b \in [k]$ be two distinct integers and $G = (V_G, E_G, lab_G) \in CW_k$ be a labeled digraph, then
	\newline
	(a) $\rho_{a \rightarrow b}(G) := (V_G, E_G, lab')$ defined by
	\newline
	$lab'(u) :=
	\begin{cases}
		lab_G(u) & \text{if } lab_G(u) \neq a\\
		b & \text{if } lab_G(u) = a
	\end{cases}$
	\newline
	for every $u \in V_G$ is in $dCW_k$ and
	\newline
	(b) $\alpha_{a, b}(G) := (V_G, E', lab_G)$ defined by $E' := E_G \cup \{(u, v) | u, v \in V_G, u \neq v, lab(u) = a, lab(v) = b\}$ is in $dCW_k$.
\end{enumerate}
The \textit{directed clique-width} of a labeled digraph $G$ is the least integer $k$ such that $G \in dCW_k$. The \textit{directed clique-width} of an unlabeled digraph $G = (V, E)$, $dcw(G)$ for short, is the smallest integer $k$, such that there is some mapping $lab : V \rightarrow [k]$ such that the labeled digraph $(V, E, lab)$ has directed clique-width at most $k$.
%Let $\mathscr{C}$ be a countable set of labels. A \textit{labeled} graph is a pair $(G, \gamma)$ where $\gamma$ maps $V(G)$ into $\mathscr{C}$. A labeled graph can also be defined as a triple $G = (V, E, \gamma)$, and its labeling function is denoted by $\gamma{}(G)$ whenever the relevant graph $G$ must be specified. We say that $G$ is $C$-\textit{labeled} if $C$ is finite and $\gamma{}(G)(V) \subseteq C$. We denote by $\overrightarrow{\mathscr{G}}(C)$ the sets of directed $C$-labeled graphs. A vertex with label $a$ will be called $a$-\textit{port}.

%We introduce the following symbols:
%\begin{itemize}
	%\item a nullary symbol $a$ for every $a \in \mathscr{C}$;
	%\item a unary symbol $\rho_{a \rightarrow b}$ for $a, b \in \mathscr{C}$ with $a \neq b$;
	%\item a unary symbol $\alpha_{a, b}$ for $a, b \in \mathscr{C}$ with $a \neq b$;
	%\item a binary symbol $\oplus$.
%\end{itemize}
%These symbols are intended to denote operations on graphs: $\rho_{a \rightarrow b}$ ``renames'' $a$ as $b$, $\alpha_{a, b}$ ``creates arcs'', $\oplus$ is the disjoint union.

%For $C \subseteq \mathscr{C}$ we denote by $\overrightarrow{T}(C)$ the set of finite well-formed terms written with the symbols $\oplus, a, \rho_{a \rightarrow b}, \alpha_{a, b}$, for $a, b \in \mathscr{C}, a \neq b$. Each term in $\overrightarrow{T}(C)$ denotes a set of directed labeled graphs.

%The definitions below are given by induction on the structure of $t \in \overrightarrow{T}(C)$. We let $val(t)$ be the set of graphs denoted by $t$.

%We have the following cases:
%\begin{enumerate}[label=(\arabic*)]
	%\item $t = a \in C$: $val(t)$ is the set of graphs with a single vertex labeled by $a$;
	%\item $t = t_1 \oplus t_2$: $val(t)$ is the set of graphs $G = G_1 \cup G_2$ where $G_1 = (V_1, E_1)$, $G_2 = (V_2, E_2)$, $V_1 \cap V_2 = \emptyset$, and $G_1 \in val(t_1)$, $G_2 \in val(t_2)$;
	%\item $t = \rho_{a \rightarrow b}(t')$: $val(t) = \{\rho_{a \rightarrow b} \ | \ G \in val(t')\}$ where for every graph $G$ in $val(t')$, the graph $\rho_{a \rightarrow b}$ is obtained by replacing in $G$ every vertex label $a$ with $b$;
	%\item $t = \alpha_{a, b}(t')$: $val(t) = \{\alpha_{a, b}(G) \ | \ G \in val(t')\}$ where for every directed labeled graph $G = (V, E, \gamma)$ in $val(t')$, we let $\alpha_{a, b}(G) = (V, E', \gamma)$ such that $E' = E \cup \{(x, y) \ | \ x, y \in V, x \neq y, \gamma(x) = a, \gamma(y) = b\}$.
%\end{enumerate}
%For the \textit{directed clique-width} of a directed labeled graph $G$, we let $cwd(G) = \min\{|C| \ | \ G \in val(t), t \in \overrightarrow{T}(C)\}$.

\paragraph{Definition 4.10 (source-to-sink square grids)} A graph $G_{n, n} = (V, E)$ is a \textit{square grid} if all vertices $v \in V$ have a distinct number $num(v)$ from 1 to $n^2$, $n^2 = |V|$, such that the following properties hold true:
\begin{enumerate}[label=(\roman*)]
	\item For all $u, v \in V$, if $num(v) = num(u) + n$, then $\{u, v\} \in E$.
	\item For all $u, v \in V$, if $num(v) = num(u) + 1$ and $num(u)$ mod $n \neq 0$, then $\{u, v\} \in E$.
	\item There are no edges aside from those with properties (i) or (ii).
\end{enumerate}
A digraph $\overrightarrow{G_{n, n}} = (V, E)$, which we refer to as \textit{source-to-sink square grid} in the further course, is a special case of square grids for which we substitute ``$\{u, v\} \in E$'' by ``$(u, v) \in E$'' in both properties (i) and (ii), and ``edges'' by ``arcs'' in property (iii).

\paragraph{Lemma 4.11} For all source-to-sink square grids $\overrightarrow{G_{n, n}}$, $n \geq 3$, $dcw(\overrightarrow{G_{n, n}}) \geq n+1$.

This can be concluded because in \cite{GR00}, it has been shown that for every square grid $G_{n, n}$, $n \geq 3$, $cw(G_{n, n}) = n + 1$, where $cw(G)$ is the undirected clique-width of an undirected graph $G$ which is elaborated in \cite{GWY16}. Moreover, as maintained by \cite{CO00} the inequality $dcw(G) \geq cw(und(G))$ holds true for a digraph $G = (V, E)$ and the underlying graph $und(G) = (V, E')$.

\paragraph{Conjecture 4.12} For all digraphs $G$ that contain multiple directed cycles, $dpw(G)$ and $dtw(G)$ can be in $\Theta(|V|)$.
\newline
\newline
We could not prove Conjecture 4.12, but the reasoning is shown below:
\newline
Let $\overleftrightarrow{K_n} = (V_K, E_K)$, $n = |V_K|$, be a complete digraph. We construct $K_n' = (V, E)$ as follows. For each $(u, w) \in E_K$, $u, w \in V_K$, we add $u, v, w$ to $V$, $v \notin V_K$, and $(u, v), (v, w)$ to $E$.
\newline
We have not found a directed path-decomposition or a tree-decomposition such that $dpw(K_n'), dtw(K_n') < n$.

\subsection{Transitive tournaments with normal coloring}
\paragraph{Directed path-width} Let $G = (V, E)$ be an A1-digraph.
	\subparagraph{Lower bound} Assuming that $G$ is a transitive tournament, i.e., acyclic, we know by Corollary 4.2 that $dpw(G) = 0$. Thus, 0 is the lower bound.
	\subparagraph{Upper bound} $dpw(G)$ is at the maximum for complete digraphs, as can be seen in Corollary 4.3. We must have a transitive tournament $\overrightarrow{T_n} = (V_T, E_T)$ as an induced subdigraph in $G$. Let us add a complete digraph $\overleftrightarrow{K_n} = (V_K, E_K)$ such that $V = V_T \cup V_K$, $E = E_T \cup E_K$, $|V_T| = |V_K|$, and $\overrightarrow{T_n}$ and $\overleftrightarrow{K_n}$ are vertex-disjoint. According to Lemma 4.4, $dpw(G) = \max\{dpw(\overrightarrow{T_n}), dpw(\overleftrightarrow{K_n})\} = \max\{0, |V_K| - 1\} = \frac{|V|}{2} - 1$. Thus, $\frac{|V|}{2} - 1$ is the upper bound.

\paragraph{Directed tree-width} Let $G = (V, E)$ be an A1-digraph.
	\subparagraph{Lower bound} Assuming that $G$ is a transitive tournament, i.e., acyclic, we know by Corollary 4.6 that $dtw(G) = 0$. Thus, 0 is the lower bound.
	\subparagraph{Upper bound} $dtw(G)$ is at the maximum for complete digraphs, as can be seen in Corollary 4.7. We must have a transitive tournament $\overrightarrow{T_n} = (V_T, E_T)$ as an induced subdigraph in $G$. Let us add a complete digraph $\overleftrightarrow{K_n} = (V_K, E_K)$ such that $V = V_T \cup V_K$, $E = E_T \cup E_K$, $|V_T| = |V_K|$, and $\overrightarrow{T_n}$ and $\overleftrightarrow{K_n}$ are vertex-disjoint. According to Lemma 4.8, $dtw(G) = \max\{dtw(\overrightarrow{T_n}), dtw(\overleftrightarrow{K_n})\} = \max\{0, |V_K| - 1\} = \frac{|V|}{2} - 1$. Thus, $\frac{|V|}{2} - 1$ is the upper bound.

\paragraph{Directed clique-width} Let $G = (V, E)$ be an A1-digraph.
	\subparagraph{Lower bound} If $E = \emptyset$, then $dcw(G)$ = 1. Thus, 1 is the lower bound.
	\subparagraph{Upper bound} An exact upper bound has not been found for $G$. However, if $G$ contains a transitive tournament $\overrightarrow{T_2}$ as well as a source-to sink square grid $\overrightarrow{G_{n, n}}$ as induced subdigraphs, $n \geq 3$, following Lemma 4.11 we can conclude that $dcw(G)$ is greater than $n$, i.e., $dcw(G)$ depends on $|V|$ because $n$ depends on $|V|$. Thus, the upper bound is in $\Theta(|V|)$.

\subsection{Transitive tournaments with good coloring}
\paragraph{Directed path-width} Let $G = (V, E)$ be an A2-digraph.
	\subparagraph{Lower bound} Assuming that $G$ is a transitive tournament, i.e., acyclic, we know by Corollary 4.2 that $dpw(G) = 0$. Thus, 0 is the lower bound.
	\subparagraph{Upper bound} We know by Lemma 3.10 that $G$ is acyclic so that by Corollary 4.2 we can conclude $dpw(G) = 0$. Thus, 0 is the upper bound.

\paragraph{Directed tree-width} Let $G = (V, E)$ be an A2-digraph.
	\subparagraph{Lower bound} Assuming that $G$ is a transitive tournament, i.e., acyclic, we know by Corollary 4.2 that $dtw(G) = 0$. Thus, 0 is the lower bound.
	\subparagraph{Upper bound} We know by Lemma 3.10 that $G$ is acyclic so that by Corollary 4.6 we can conclude $dtw(G) = 0$. Thus, 0 is the upper bound.

\paragraph{Directed clique-width} Let $G = (V, E)$ be an A2-digraph.
	\subparagraph{Lower bound} If $E = \emptyset$, then $dcw(G)$ = 1. Thus, 1 is the lower bound.
	\subparagraph{Upper bound} An exact upper bound has not been found for $G$. However, if $G$ contains a transitive tournament $\overrightarrow{T_{n+2}}$ as well as a source-to sink square grid $\overrightarrow{G_{n, n}}$ as induced subdigraphs, $n \geq 3$, following Lemma 4.11 we can conclude that $dcw(G)$ is greater than $n$, i.e., $dcw(G)$ depends on $|V|$ because $n$ depends on $|V|$. Thus, the upper bound is in $\Theta(|V|)$.

\subsection{Complete digraphs with normal coloring}
\paragraph{Directed path-width} Let $G = (V, E)$ be a B1-digraph.
	\subparagraph{Lower bound} Assuming that the complete digraph $\overleftrightarrow{K_n} = G[V']$, $n = \overrightarrow{\omega_B}(G)$ and $V' \subseteq V$, along with Corollary 4.3 we can conclude $|V| \geq dpw(G) \geq \overrightarrow{\omega_B}(G)$. Thus, $\overrightarrow{\omega_B}(G)$ is the lower bound.
	\subparagraph{Upper bound} Assuming that $G$ is a complete digraph, we know by Corollary 4.3 that $dpw(G) = |V| - 1$. Thus, $|V| - 1$ is the upper bound as $|V|$ cannot be reached due to the fact that we always subtract 1 from the width of a directed path-decomposition.

\paragraph{Directed tree-width} Let $G = (V, E)$ be a B1-digraph.
	\subparagraph{Lower bound} Assuming that the complete digraph $\overleftrightarrow{K_n} = G[V']$, $n = \overrightarrow{\omega_B}(G)$ and $V' \subseteq V$, along with Corollary 4.7 we can conclude $|V| \geq dtw(G) \geq \overrightarrow{\omega_B}(G)$. Thus, $\overrightarrow{\omega_B}(G)$ is the lower bound.
	\subparagraph{Upper bound} Assuming that $G$ is a complete digraph, we know by Corollary 4.7 that $dtw(G) = |V| - 1$. Thus, $|V| - 1$ is the upper bound as $|V|$ cannot be reached due to the fact that we always subtract 1 from the width of a tree-decomposition.

\paragraph{Directed clique-width} Let $G = (V, E)$ be a B1-digraph.
	\subparagraph{Lower bound} If $E = \emptyset$, then $dcw(G)$ = 1. Thus, 1 is the lower bound.
	\subparagraph{Upper bound} An exact upper bound has not been found for $G$. However, if $G$ contains a complete digraph $\overleftrightarrow{K_2}$ as well as a source-to sink square grid $\overrightarrow{G_{n, n}}$ as induced subdigraphs, $n \geq 3$, following Lemma 4.11 we can conclude that $dcw(G)$ is greater than $n$, i.e., $dcw(G)$ depends on $|V|$ because $n$ depends on $|V|$. Thus, the upper bound is in $\Theta(|V|)$.

\subsection{Underlying graphs with normal coloring}
\paragraph{Directed path-width} Let $G = (V, E)$ be a C1-digraph.
	\subparagraph{Lower bound} Since every A1-digraph is a C1-digraph as shown in Lemma 3.8, we can use the same explanation for $G$. Thus, 0 is a lower bound.
	\subparagraph{Upper bound} Since every B1-digraph is a C1-digraph as shown in Lemma 3.8, we can use the same explanation for $G$. Thus, $|V| - 1$ is an upper bound.

\paragraph{Directed tree-width} Let $G = (V, E)$ be a C1-digraph.
	\subparagraph{Lower bound} Since every A1-digraph is a C1-digraph as shown in Lemma 3.8, we can use the same explanation for $G$. Thus, 0 is a lower bound.
	\subparagraph{Upper bound} Since every B1-digraph is a C1-digraph as shown in Lemma 3.8, we can use the same explanation for $G$. Thus, $|V| - 1$ is an upper bound.

\paragraph{Directed clique-width} Let $G = (V, E)$ be a C1-digraph.
	\subparagraph{Lower bound} If $E = \emptyset$, then $dcw(G)$ = 1. Thus, 1 is the lower bound.
	\subparagraph{Upper bound} An exact upper bound has not been found for $G$. However, if $G$ contains a transitive tournament $\overrightarrow{T_2}$ as well as a source-to sink square grid $\overrightarrow{G_{n, n}}$ as induced subdigraphs, $n \geq 3$, following Lemma 4.11 we can conclude that $dcw(G)$ is greater than $n$, i.e., $dcw(G)$ depends on $|V|$ because $n$ depends on $|V|$. Thus, the upper bound is in $\Theta(|V|)$.

\subsection{Underlying graphs with good coloring}
\paragraph{Directed path-width} Let $G = (V, E)$ be a C2-digraph.
	\subparagraph{Lower bound} Assuming that $G$ is a transitive tournament, i.e., acyclic, we know by Corollary 4.2 that $dpw(G) = 0$. Thus, 0 is the lower bound.
	\subparagraph{Upper bound} We assume that Conjectures 3.11 and 4.12 are true. By Conjecture 3.11, we can have multiple directed cycles in $G$ as subdigraphs such that by Conjecture 4.12, the upper bound would be in $\Theta(|V|)$.

\paragraph{Directed tree-width} Let $G = (V, E)$ be a C2-digraph.
	\subparagraph{Lower bound} Assuming that $G$ is a transitive tournament, i.e., acyclic, we know by Corollary 4.2 that $dpw(G) = 0$. Thus, 0 is the lower bound.
	\subparagraph{Upper bound}  We assume that Conjectures 3.11 and 4.12 are true. By Conjecture 3.11, we can have multiple directed cycles in $G$ as subdigraphs such that by Conjecture 4.12, the upper bound would be in $\Theta(|V|)$.

\paragraph{Directed clique-width} Let $G = (V, E)$ be a C2-digraph.
	\subparagraph{Lower bound} If $E = \emptyset$, then $dcw(G)$ = 1. Thus, 1 is the lower bound.
	\subparagraph{Upper bound} An exact upper bound has not been found for $G$. However, if $G$ contains a transitive tournament $\overrightarrow{T_{n+2}}$ as well as a source-to sink square grid $\overrightarrow{G_{n, n}}$ as induced subdigraphs, $n \geq 3$, following Lemma 4.11 we can conclude that $dcw(G)$ is greater than $n$, i.e., $dcw(G)$ depends on $|V|$ because $n$ depends on $|V|$. Thus, the upper bound is in $\Theta(|V|)$.

\section{Conclusion and outlook}\label{fifth-section}
We introduced five possibilities for defining perfect digraphs as can be seen in the table below, in which the results of this paper are summarized. We can see that there are two cells that are marked with ``?''. This is due to Conjectures 3.11 and 4.12; we leave them for future investigation. The same goes for the other cells with entry $\Theta(|V|)$.

Normal coloring in our three directed clique variations causes the three analyzed digraph parameters to be unbounded, just like the three equivalent graph parameters are in undirected perfect graphs. Still, there might be different results for other digraph parameters.

Transitive tournaments (and perhaps underlying graphs) as directed cliques in combination with good coloring have upper bounds for directed path-width and directed tree-width that are not depending on the number of vertices, i.e., the parameters are bounded. In \cite{GWY16} it is stated that these parameters, when bounded, allow to give polynomial algorithms for a number of hard problems, hence, they seem promising and could be considered in future work.

\begin{table}
	%\begin{tabular}{|p{5cm}|p{9cm}|}
	\begin{tabular}{|p{3.4cm}|p{0.8cm}|p{1cm}|>{\centering\arraybackslash}p{2.4cm}|>{\centering\arraybackslash}p{2.4cm}|}
		\hline
		directed clique & \multicolumn{2}{l|}{parameters}  & normal coloring & good coloring \\ \hline
		transitive tournament & dpw & min & 0 & 0 \\ \cline{3-5}
		&& max & $\frac{|V|}{2} - 1$ & 0 \\ \cline{2-5}
		& dtw & min & 0 & 0 \\ \cline{3-5}
		&& max & $\frac{|V|}{2} - 1$ & 0 \\ \cline{2-5}
		& dcw & min & 1 & 1 \\ \cline{3-5}
		&& max & $\Theta(|V|)$ & $\Theta(|V|)$ \\ \hline
		complete digraph & dpw & min & $\overrightarrow{\omega_B}(G)$ & - \\ \cline{3-5}
		&& max & $|V| - 1$ & - \\ \cline{2-5}
		& dtw & min & $\overrightarrow{\omega_B}(G)$ & - \\ \cline{3-5}
		&& max & $|V| - 1$ & - \\ \cline{2-5}
		& dcw & min & 1 & - \\ \cline{3-5}
		&& max & $\Theta(|V|)$ & - \\ \hline
		underlying graph & dpw & min & 0 & 0 \\ \cline{3-5}
		&& max & $|V| - 1$ & $\Theta(|V|)$ (?) \\ \cline{2-5}
		& dtw & min & 0 & 0 \\ \cline{3-5}
		&& max & $|V| - 1$ & $\Theta(|V|)$ (?) \\ \cline{2-5}
		& dcw & min & 1 & 1 \\ \cline{3-5}
		&& max & $\Theta(|V|)$ & $\Theta(|V|)$ \\ \hline
	\end{tabular}
	\caption{Results} 
\end{table}
\bibliographystyle{alphadin}
\bibliography{refs}
\end{document}